const choo = require('choo')
const html = require('choo/html')
const css = require('sheetify')
const app = choo()

//screens
const welcome = require('./screens/welcome.js')
const message = require('./screens/message.js')
const thanks = require('./screens/thanks.js')

const style = css("./main.css")
const reset = css("./reset.css")

app.use((state,emitter) =>{
  // get pregunta
  state.pregunta = ''
  fetch('/pregunta', {method:'GET'})
    .then(res =>{
      res.json().then(data =>{
        state.pregunta = data.pregunta
        emitter.emit('render')
      })
      .catch(err => console.log('error parseando json', err))
    })
    .catch(err => console.log('error pidiendo pregunta', err))

  //screen logic
  state.SCREENS = [welcome,message,thanks]
  state.fondoPositions = ["0px", "-2250px", "-4200px"]
  state.atScreen = 0
  state.fondoPosition = state.fondoPositions[state.atScreen]
  state.remove = false
  state.once = false

  emitter.on('nextScreen', () =>{
    // logica para poder animar la salida de las pantallas
    //
    state.remove = true
    emitter.emit('render')

    setTimeout(() =>{
      state.remove = false
      state.atScreen = (state.atScreen + 1) % 3

      // LOLO: de última a primer pantalla se mueve sola a los 5 segundos
      if(state.atScreen === 2){
        setTimeout(() =>{
          emitter.emit('nextScreen')
        }, 5000)
      }
      state.fondoPosition = state.fondoPositions[state.atScreen]
      emitter.emit('render')
    },200)
  })

  emitter.on('reset', () =>{
    state.atScreen = 0
    state.fondoPosition = state.fondoPositions[state.atScreen]
    emitter.emit('render')
  })

})
app.use(require("./keyboard.js"))
app.use(require('./messages.js'))
app.use(require('choo-devtools')())

const fondo = css`
:host {
  height:100%;
  width:100%;
  position:absolute;
  top:0;
  left:0;
  z-index:-1;
}

:host > img {
  height:100%;

  /* LOLO --> ANIMACION DE MOVIMIENTO DE LAS PANTALLAS */
  transition: transform 2s cubic-bezier(.69,.75,.3,1.36); 

}
`

const chica = css`
:host {
  position:absolute;
  left:0;
  top:148px;
  z-index:1;
  width:100%;
}

`

app.route("/", (state,emit) =>{
  return html`
  <body class=${style + " " + reset}>
    <img class=${chica} src="assets/img/chica.png" />
    <div class=${fondo} id="fondo">
      <img src="assets/img/background.png" 
        style="transform:translateX(${state.fondoPosition});" />
    </div>
    ${state.SCREENS[state.atScreen](state,emit)}
  </body>
  `
})
app.route("/test", require('./screens/test.js'))
app.route("/moderar", require('./screens/moderar.js'))
app.mount('body')
