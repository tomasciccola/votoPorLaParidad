const bankai = require('bankai/http')
const http = require('http')
const path = require('path')
const fs = require('fs')
const uuid = require('uuid').v4
const PORT = 8080

const db = require('./db.json')
const config = require('./config.json') 

const parsePost = (req,onEnd) =>{
  let body = ''
  req.on('data', chunk =>{
    body += chunk
  })
  req.on('end', () =>{
    onEnd(JSON.parse(body))
  })
}

var compiler = bankai(path.join(__dirname, 'client.js'))

var server = http.createServer((req,res) =>{

  //console.log(req.url, req.method)

  // POST MESSAGE
  if(req.url === "/message" && req.method === "POST"){
    parsePost(req, json => {
      console.log(json)
      json.id = uuid()
      db.push(json)
      fs.writeFileSync('db.json', JSON.stringify(db,false,4))
      res.end('ok!')
    })

    // GET MESSAGES
  }else if(req.url === "/messages" && req.method === "GET"){
    res.setHeader('Content-Type', 'application/json');
    let obj = {mensajes:db, pregunta:config.pregunta}
    res.end(JSON.stringify(obj));

    // DELETE MESSAGES 
  }else if(req.url === "/delete" && req.method === 'POST'){
    parsePost(req, json => {
      console.log('deleting', db[json.idx])
      db.splice(json.idx, 1);
      fs.writeFileSync('db.json', JSON.stringify(db,false,4))
      res.end('ok!')
    })

    // PREGUNTA
  }else if(req.url === "/pregunta") {
    // GET
    if(req.method === 'GET'){
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(config))
    }
    //POST
    if(req.method === 'POST'){
      parsePost(req, json =>{
        console.log('cambiando pregunta', json)
        config.pregunta = json.pregunta
        fs.writeFileSync('config.json', JSON.stringify(config,false,4))
        res.end('ok!')
      })
    }
    // BORRAR DB
  }else if(req.method === 'GET' && req.url === '/borrarTodo'){
    console.log('borrar todo!')
    fs.writeFileSync(`db_${uuid()}.json`, JSON.stringify(db, false, 4))
    db.length = 0
    fs.writeFileSync('db.json', JSON.stringify(db,false,4))
    res.end('ok')
    // SERVE WEB
  }else{
    compiler(req,res, () => {
      res.statusCode = 404
      res.end('not found')
    })

  }

})

server.listen(PORT, () => {
  console.log(`server listening on ${PORT}`)
})

