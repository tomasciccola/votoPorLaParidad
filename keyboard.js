const Keyboard = require('simple-keyboard').default;

const layout = {
 'default':[
          'q w e r t y u i o p',
          'a s d f g h j k l ñ',
          '{lock} z x c v b n m {bksp}',
          '{num} {space} {enter} {send}'
        ] 
}

const layoutShift = {
  'default': [
          'Q W E R T Y U I O P',
          'A S D F G H J K L Ñ',
          '{lock} Z X C V B N M {bksp}',
          '{num} {space} {enter} {send}'
        ]
}

const layoutNum = {
  'default': [
          '1 2 3 4 5 6 7 8 9 0 {bksp}',
          '{num} {space} {enter} {send}'
        ]
}

module.exports = (state,emitter) => {

  state.shift = false
  emitter.on('shift', () =>{
    state.shift = !state.shift
  })

  state.numpad = false
  emitter.on('numpad', () =>{
    state.numpad = !state.numpad
  })

  const onChange = input => {
    document.querySelector(".input").value = input;
    document.querySelector(".input").value = input;
    //console.log("Input changed", input);
  }

  const onKeyPress = button => {
    //console.log("Button pressed", button);
    // enviar
    if(button === "{send}"){
      var target = document.querySelector("#postMessage")
      var input = document.querySelector("#mensaje")
      if(input.value.length !== 0){
        emitter.emit('newMessage', target)
      }else{
        input.value ='el mensaje no puede quedar vacío!' 
        setTimeout(() =>{
          input.value = ''
        }, 1000)
      }
    }

    // capslock  
    if(button === "{lock}") {
      emitter.emit('shift')
      //console.log('shift', state.shift)
      state.keyboard.setOptions({
        layout: state.shift ? layoutShift : layout
      })
    }

    if(button === "{num}") {
      emitter.emit('numpad')
      state.keyboard.setOptions({
        layout: state.numpad ? layoutNum : layout
      })
    }
  }

  emitter.on('nextScreen', () =>{
    if(state.atScreen === 0) {
      setTimeout(() =>{
        state.keyboard = new Keyboard({
          onChange: input => onChange(input),
          onKeyPress: button => onKeyPress(button),
          layout: layout, 
          display:{
            '{enter}' :'intro',
            '{bksp}' : 'borrar',
            '{space}' :'espacio',
            '{send}' : 'enviar',
            '{num}' : '123'
          },
          mergeDisplay:true
        });


      }, 1000) 

    }
  })



  
  
}
