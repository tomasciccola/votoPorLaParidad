module.exports = (state,emitter) => {
  state.mensajes = []

  emitter.on('fetchMessages', () =>{
    fetch('/messages', {method:'GET'})
      .then(res =>{
        if(!res.ok)console.log('error pidiendo mensajes!')
        res.json().then(data =>{
          // console.log('data', data)
          state.mensajes = data.mensajes
          state.pregunta = data.pregunta
          emitter.emit('render')
        })
          .catch(e => console.log('error parseando json',e))

      })
      .catch(e => console.log('error pidiendo mensajes!', e))

  })


  emitter.on('newMessage', form =>{
    var data = new FormData(form)
    var headers = new Headers({ 'Content-Type': 'application/json' })  
    var body = {}
    for(let pair of data.entries()) body[pair[0]] = pair[1]
    body.pregunta = state.pregunta
    console.log('body', body)
    body = JSON.stringify(body)
    fetch('/message', {method:'POST', body, headers})
      .then(res =>{
        if(!res.ok) console.log('error en el post!')
        console.log('request ok!')

        if(state.route === '/'){
          emitter.emit('nextScreen')
        }else{
          emitter.emit('render')
        }
      })
      .catch(e =>console.log('error en el post!', e))
  })

  emitter.on('deleteMessage', idx => {
    var headers = new Headers({ 'Content-Type': 'application/json' })  
    var body = {idx : idx}
    body = JSON.stringify(body)
    fetch('/delete', {method:'POST', body, headers})
      .then(res =>{
        if(!res.ok) console.log('error en el post!')
        console.log('request ok!')
        emitter.emit('fetchMessages')
      })
      .catch(e => console.log('error en el post!', e))
  })

  emitter.on('deleteMessages', () => {
    fetch('/borrarTodo', {method:'GET'})
      .then(res => {
        if(!res.ok) console.log('error borrando db!')
        console.log('request ok!')
        emitter.emit('fetchMessages')
      })
      .catch(e => console.log('error borrando db', e))
  })

  emitter.on('guardarPregunta', p => {
    console.log('nueva pregunta', p)
    var headers = new Headers({ 'Content-Type': 'application/json' })  
    var body = {pregunta:p}
    body = JSON.stringify(body)
    fetch("/pregunta", {method:'POST', body,headers})
      .then(res =>{
        if(!res.ok) console.log('error en el post!')
        console.log('request ok!')
        emitter.emit('fetchMessages')
      })
      .catch(e => console.log('error en el post!', e))
  })



  if(state.route === 'moderar'){
    emitter.emit('fetchMessages')
  }
}
