const css = require('sheetify')
const html = require('choo/html')
const keyboardStyle = css("../node_modules/simple-keyboard/build/css/index.css")

const style = css`
:host > form textarea {
  position:absolute;
  left:37%;
  top:44%;
  width:60%;
  height:25%;
  resize:none;
  border:none;
  outline:none;
  background-color:#d3d93b;
  color:#004752;
  z-index:3;
  font-family:encode;
  font-size:48px;

  animation:textarea 2s;
}

@keyframes textarea {
  0%{
    opacity:0;
  }

  50% {
    opacity:0;
  }

  100%{
    opacity:1;
  }
}


:host > form textarea:focus {
  outline:none;
}

:host > form #btnEnviar {
  position:absolute;
  right:10%;
  bottom:0;
  border:2px solid #004752;
  font-size:32px;
  background:none;
  cursor:pointer;
  z-index:3;
}

:host > #nube {
  position:absolute;
  left:25%;
  width:55%;
  top:5%;

  /* LOLO --> ANIMACIÓN DE LA NUBE DE TEXTO */
  animation:nube 1.7s ease-out;
  transition:transform 0.5s ease-out;
}

@keyframes nube {
  0%{
    transform:translateY(-1000px);
    opacity:0;
  }

  100%{
    transform:translateY(0px);
    opacity:1;
  }

}

:host > #quote{
  position:absolute;
  width:7%;
  left:37%;
  top:30%;

  /* LOLO --> ANIMACIÓN DEL QUOTE */
  animation:quote 1.5s ease-out;
  transition:transform 0.8s ease-out;
  /*animation-delay:0.3s;*/
}


@keyframes quote {
  0%{
    transform:translateX(4920px);
  }

  100%{
    transform:translateX(0px);
  }

}

:host > #pregunta {
  font-family:encodeBold;
  font-size:32px;
  position:absolute;
  top:15%;
  left:34%;
  color:#004752;
  width:40%;
  height:25%;
  z-index:2;

  /* LOLO --> ANIMACIÓN DEL TEXTO DE LA NUBE */
  animation:pregunta 1.7s ease-out;
  transition:transform 0.4s ease-out;
  /*animation-delay:0.15s;*/
}

@keyframes pregunta {
  0%{
    transform:translateY(-2500px);
    opacity:0;
  }

  100%{
    transform:translateY(0px);
    opacity:1;
  }

}

:host > div.simple-keyboard {
  position:absolute;
  bottom:8px;
  color:black;
  font-size:16px;
  width:30%;
  z-index:3;
  position:absolute;
  left:35%;
  background-color:#4bb7e8;
  font-family:encode;

  /* LOLO --> ANIMACIÓN DEL TECLADO */
  animation:keyboard 1.2s ease-out;
  animation-delay:0.7s;
}

@keyframes keyboard {
  0%{
    transform:translateY(1000px);
  }

  100%{
    transform:translateY(0px);
  }
}

:host > #reset {
position:absolute;
top:40px;
cursor:pointer;
opacity:0;
right:14px;
background:none;
width:100px;
height:100px;
}

:host > .simple-keyboard .hg-button {
  border-radius:0px;
  background-color:#e8e8e6;
  color:#004752;
  font-size:24px;
  font-family:encodeBold;
  height:50px;
}

:host > .simple-keyboard .hg-button-space {
  width:40%;
}

:host > .simple-keyboard .hg-button-num {
  width:10%;
}

:host > .simple-keyboard .hg-button-enter {
  width:12%;
}

:host > .simple-keyboard .hg-button-send {
  color:#e8e8e6;
  background-color:#004752;
}

`

module.exports = function(state,emit){

  const onsubmit = e =>{
    e.preventDefault()
    emit('newMessage', e.currentTarget)
  }

  const reset = e =>{
    emit('reset')
  }

  let transition = `transform:translateX(${state.remove ? "-1920px" : "0px"})`

  return html`
    <div class=${style}>
      <p id="pregunta" style=${transition}>
        ${state.pregunta}
      </p>
      <img id="nube" style=${transition} src="assets/img/nube.png" />
      <img id="quote" style=${transition} src="assets/img/quote.png" />
      <form id="postMessage" onsubmit=${onsubmit}>
        <textarea class="input" id="mensaje" name="mensaje" type="text" required ></textarea>
      </form>  
      <div class=${"simple-keyboard" + " " + keyboardStyle} style=${transition}></div>
      <button id="reset" onclick=${reset}>reset</button>
    </div>
  `

}

