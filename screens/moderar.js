const css = require('sheetify')
const html = require('choo/html')

const mainStyle = css("../main.css")
const reset = css("../reset.css")

const style = css`

:host {
  overflow-x:hidden;
  overflow-y:hidden;
  background-color:#4db6e5;
  width:100vw;
  height:100vh;
  margin:0 auto;
  padding:0;
}

:host > div#pregunta {
  padding:24px;
  display:flex;
  flex-direction:column;
  width:90%;
  background-color:#014753;
  color:white;
}

#tituloconsigna {
  font-family:encodeBold;
  font-size:32px;
}

#pregunta button {
  font-family:encodeBold;
  border:1px solid #014753;
  background-color:#d5d340;
  color:#014753;
  font-size:24px;
  padding:20px;
  width:20%;
  text-decoration:none;
  cursor:pointer;
  /*align-self:center;*/
  margin-top:24px;
  text-transform:uppercase;
}

#pregunta textarea {
  resize:none;
  height:120px;
  border:none;
  outline:none;
  font-family:encode;
  font-size:18;
  background:none;
  margin-top:32px;
  color:white;
  width:70%;
}

#mensajes {
  background-color:#e8e8e6;
  width:90%;
  height:50%;
  padding:24px;
  overflow-y:scroll;
}

#respuestas {
  display:flex;
  flex-direction:row;
  align-items:end;
}

#respuestas h2 {
  font-family:encodeBold;
  font-size:32px;
  color:#014753;
  margin-bottom:24px;
}

#btnBorrar {
  font-family:encodeBold;
  background-color:#d5d340;
  border:none;
  color:#014753;
  font-size:24px;
  padding:20px;
  text-decoration:none;
  cursor:pointer;
  align-self:center;
}

`

const msgStyle = css`
:host {
  display:flex;
  flex-direction:row;
  justify-content:flex-begin;
  align-items:center;
  text-align:left;
  width:100%;
  border-bottom:2px solid black;
  margin-bottom: 10px;
  padding-bottom:10px
}
:host > p {
  width:90%;
  font-family:encode;
  font-size:16px;
}

:host > button {
  background:none;
  height:50%;
  text-decoration:none;
  cursor:pointer;
  border:1px solid #014753;
  background-color:#d5d330;
  color:#014753;
  font-size:12px;
  padding:20px;
}
`

const drawMensaje = (emit) => (msg,idx) => {

  const delmsg = () =>{
    emit('deleteMessage', idx)
  }

  return html`
  <div id=${"msg_" + idx} class=${msgStyle}>
   <p>${msg.mensaje}</p> 
   <button onclick=${delmsg}>X</button>
  </div>
  `
}

module.exports = function(state,emit){
  const guardarPregunta = _ => emit('guardarPregunta', state.pregunta) 
  const borrarMensajes = _ => confirm("¿Estás segurx?") ? emit('deleteMessages') : ''
  const onPreguntaChange = e => state.pregunta = e.target.value

  return html`
    <body class=${mainStyle + " " + style + " " + reset}>
      <div id="pregunta">
        <h2 id="tituloconsigna">CONSIGNA:</h2>
        <textarea onkeyup=${onPreguntaChange}>${state.pregunta.replaceAll("\n", "")}</textarea>
        <button onclick=${guardarPregunta}> guardar </button>
      </div>
      <div id="mensajes">
        <div id="respuestas">
          <h2>RESPUESTAS</h2>
        </div>
        ${state.mensajes.map(drawMensaje(emit))}
        <button id="btnBorrar" onclick=${borrarMensajes}> borrar todo </button>
      </div>
    </body>
  `

}
