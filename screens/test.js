const html = require('choo/html')
const css = require('sheetify')

const style = css `
:host {
  margin: 0 auto;
  width:50%;
  font-family:monospace;
  font-size:28px;
  background-color:#1c1c1c;
  padding:80px;
  color:white;
}
:host > form {
  display:flex;
  flex-direction:column;
}

:host > form textarea{
  resize:none;
  border:1px solid white;
  font-size:32px;
  background:none;
  color:white;
  height:240px;
}

:host > form input {
  text-decoration:none;
  border: 1px solid white;
  font-size:32px;
  background:none;
  color:white;
}
:host > p {
  margin-bottom:12px;
}
`

module.exports =  (state,emit) => {

  const onsubmit = e =>{
    e.preventDefault()
    emit('newMessage', e.currentTarget)
  }

  const oninput = e => {
    if(e.inputType === "insertLineBreak"){
      var form = document.querySelector('#postMessage')
      console.log('newMessage', form)
      emit('newMessage', form)
    }
  }

  setTimeout(() =>{
    var area = document.querySelector('textarea')
    area.focus()
  }, 1000)

  return html`
    <body class=${style}>
      <p> Con enter podés mandar el texto directamente </p> 
      <form id="postMessage" onsubmit=${onsubmit}>
        <textarea oninput=${oninput} id="mensaje" autofocus name="mensaje" type="text"></textarea>
        <input type="submit" value="Mandar">
      </form>  
    </body>
  `
}
