const css = require('sheetify')
const html = require('choo/html')

const style = css`
:host #flechas {
  position:absolute;
  left:40%;
  top:55%;

  /* LOLO --> ANIMACIÓN DE LA FLECHA */
  animation:flechas 1.9s;
  animation-delay: 0.5;
  animation-timing-function:ease-out;
}

@keyframes flechas {
  0%{
    transform:translateX(2200px);
    opacity:0;
  }

  100%{
    transform:translateX(0px);
    opacity:1;
  }
}

:host #patria {
  position:absolute;
  left:40%;
  top:20%;
  width:50%;

  /* LOLO --> ANIMACIÓN DEL TEXTO */
  animation:patria 1.7s;
  animation-timing-function:ease-out;

}

@keyframes patria {
  0%{
    transform:translateY(-1200px);
    opacity:0;
  }

  100%{
    transform:translateY(0px);
    opacity:1;
  }
}

`

module.exports = function(state,emit){
  return html`
    <div class=${style}>
      <img id="flechas" src="assets/img/flechas2.png"/>
      <img id="patria" src="assets/img/patria.png"/>
    </div>
  `

}
