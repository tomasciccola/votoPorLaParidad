const css = require('sheetify')
const html = require('choo/html')

const style = css`

:host #hola {
  position:absolute;
  text-transform:capitalize;
  font-size:381px;
  left:140px;
  top:-10px;
  font-family:encodeBold;
  color:#e8e3e6;
  z-index:-1;

  /* LOLO --> ANIMACIÓN DEL "¡HOLA!" */
  animation: holaMove 1.2s;
  animation-timing-function:ease-out;
  transition:transform 0.5s ease-out;
}

@keyframes holaMove {

  0%{
    transform:translateY(-1400px);
  }
  100%{
    transform:translateY(0px);
  }

}

:host #llamado {
  font-family:encode;
  position:absolute;
  color:#d3d93b;
  font-size:48px;
  top:50%;
  left:32%;
  /*width:25%;*/

  /* LOLO --> ANIMACIÓN DEL PÁRRAFO DE TEXTO */
  animation:llamado 2s;
  transition:transform 0.8s ease-out;
}

@keyframes llamado {
  0%{
    opacity:0;
  }

  100%{
    opacity:1;
  }
}


:host #next {
  position:absolute;
  top:75%;
  left:30%;
  width:30%;
  height:20%;
  opacity:0;
  cursor:pointer;
  z-index:2;

}

:host #flecha {
  position:absolute;
  left:32%;
  top:78%;
  width:12%;
  z-index:2;

  /* LOLO --> ANIMACIÓN DE LA FLECHA */
  animation: flechaMove 1.0s;
  animation-timing-function:ease-out;
  transition:transform 0.6s ease-in;
}

@keyframes flechaMove {

  0%{
    transform:translateX(-1400px);
  }
  100%{
    transform:translateY(0px);
  }

}

`
module.exports = function(state,emit){
  let transition = `transform:translateX(${state.remove ? "-1920px" : "none"})`
  return html`
    <div class=${style}>
    <p id="hola" style=${transition}> ¡hola! </p>
    <p id="llamado" style=${transition}> 
      ¡Vení a crear tu propia ley  <br>
      para transformar las  <br>
      desigualdades entre los  <br>
      géneros!
    </p>
    <img id="flecha" src="assets/img/flechas.png" style=${transition} />
    <button id="next" onclick=${next}> 
    </button>
    </div>
  `

  function next(){
    console.log('next')
    emit('nextScreen')
  }
}
